var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
];

function baca(sisa, books, i) {
    readBooks(sisa, books[i], function(waktu) {
        var berikutnya = i + 1;
        if (berikutnya < books.length) {
            baca(waktu, books, berikutnya);
        }
    });
}

baca(10000, books, 0);
// jawaban soal 1
