//soal 1 function penghitung jumlah kata
function jumlah_kata(kalimat) {
    var kata = kalimat.split(" ");
    return kata.length
}

kalimat_1 = "Halo nama saya Farhan Hadiansyah"
kalimat_2 = "Saya Farhan"
kalimat_3 = "Saya Farhan Hadiansyah"

console.log(jumlah_kata(kalimat_1)) // 5
console.log(jumlah_kata(kalimat_2)) // 2
console.log(jumlah_kata(kalimat_3)) // 3

// soal 2 penghasil tanggal hari esok
function next_date(dd,mm,yy){
    var kata_bulan
    if(mm==1 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==2 & dd==29 & yy % 4 == 0|| mm==2 & dd==28 & yy%4!=0){
        mm=mm+1
        dd=1
    }else if (mm==3 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==4 & dd==30){
        mm=mm+1
        dd=1
    }else if (mm==5 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==6 & dd==30){
        mm=mm+1
        dd=1
    }else if (mm==7 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==8 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==9 & dd==30){
        mm=mm+1
        dd=1
    }else if (mm==10 & dd==31){
        mm=mm+1
        dd=1
    }else if (mm==11 & dd==30){
        mm=mm+1
        dd=1
    }else if (mm==12 & dd==31){
        mm=1
        dd=1
        yy= yy+1
    }else{
        dd= dd+1
    }
    switch (mm){
        case 1  : {kata_bulan="Januari";break;}
        case 2  : {kata_bulan="Februari";break;}
        case 3  : {kata_bulan="Maret";break;}
        case 4  : {kata_bulan="April";break;}
        case 5  : {kata_bulan="Mei";break;}
        case 6  : {kata_bulan="Juni";break;}
        case 7  : {kata_bulan="Juli";break;}
        case 8  : {kata_bulan="Agustus";break;}
        case 9  : {kata_bulan="September";break;}
        case 10 : {kata_bulan="Oktober";break;}
        case 11 : {kata_bulan="November";break;}
        case 12 : {kata_bulan="Desember";break;}
    }
    console.log (dd,kata_bulan,yy)
}
var tanggal = 27
var bulan = 3
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 27 Maret 2020
var tanggal = 10
var bulan = 6
var tahun = 2019
next_date(tanggal , bulan , tahun ) // output : 10 Juni 2019
var tanggal = 29
var bulan = 7
var tahun = 2002
next_date(tanggal , bulan , tahun ) // output : 29 Juli 2002