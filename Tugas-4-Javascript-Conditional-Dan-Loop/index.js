// soal 1
var nilai = 97 


if (nilai >= 85) 
{
    console.log (("Jadi, Nilai"), nilai, ("indeksnya"), "A")
}
else if (nilai >= 75 && nilai < 85)
{
    console.log (("Jadi, Nilai"), nilai, ("indeksnya"), "B")
}
else if (nilai >= 65 && nilai < 75) 
{   
    console.log (("Jadi, Nilai"), nilai, ("indeksnya"), "c")
}
else if (nilai >= 55 && nilai < 65)
{
    console.log (("Jadi, Nilai"), nilai, ("indeksnya"), "D")
}
else if (nilai < 55)
{
    console.log (("Jadi, Nilai"), nilai, ("indeksnya"), "E")
}                        // jawaban soal 1

console.log ('\n')

// soal 2
var tanggal = 29;
var bulan = 7;
var tahun = 2002;

switch (bulan) {
    case 1:
      bulan = 'Januari';
      break;
    case 2:
      bulan = 'Februari';
      break;
    case 3:
      bulan = 'Maret';
      break;
    case 4:
      bulan = 'April';
      break;
    case 5:
      bulan = 'Mei';
      break;
    case 6:
      bulan = 'Juni';
      break;
    case 7:
      bulan = 'Juli';
      break;
    case 8:
      bulan = 'Agustus';
      break;
    case 9:
      bulan = 'September';
      break;
    case 10:
      bulan = 'Oktober';
      break;
    case 11:
      bulan = 'November';
      break;
    case 12:
      bulan = 'Desember';
      break;
  }
  var tampilTanggal = tanggal + ' ' + bulan + ' ' + tahun;
  console.log(tampilTanggal); // jawaban soal 2

  console.log ('\n')

// soal 3
console.log ('Output untuk n=3 :')
var rows = 3;
var output = '';
for (var i = 1; i <= rows; i++) {
    for (var j = 1; j <= i; j++) {
        output += '#';
    }
    console.log(output);
    output = '';
}
console.log ('Output untuk n=7 :')
var rows2 = 7;
var output2 = '';
for (var i = 1; i <= rows2; i++) {
    for (var j = 1; j <= i; j++) {
        output2 += '#';
    }
    console.log(output2);
    output2 = '';
}               // jawaban soal 3
console.log ('\n')

// soal 4
var m = 3;
var prog = ("programming");
var java = ("javascript");
var vue = ("vue js");
for (var i = 1; i <= m; i++) {
    console.log(i + " - I love " + prog );
for (var i = 2; i <= m; i++) {
    console.log(i + " - I love " + java );
for (var i = 3; i <= m; i++) {
    console.log(i + " - I love " + vue );
  if (i % 3 === 0) {
    console.log('===');
  }
}
}
}
var m = 6;
var prog = ("programming");
var java = ("javascript");
var vue = ("vue js");
for (var i = 4; i <= m; i++) {
    console.log(i + " - I love " + prog );
for (var i = 5; i <= m; i++) {
    console.log(i + " - I love " + java );
for (var i = 6; i <= m; i++) {
    console.log(i + " - I love " + vue );
  if (i % 3 === 0) {
    console.log('======');
  }
}
}
}
var m = 9;
var prog = ("programming");
var java = ("javascript");
var vue = ("vue js");
for (var i = 7; i <= m; i++) {
    console.log(i + " - I love " + prog );
for (var i = 8; i <= m; i++) {
    console.log(i + " - I love " + java );
for (var i = 9; i <= m; i++) {
    console.log(i + " - I love " + vue );
  if (i % 3 === 0) {
    console.log('==========');
  }
}
}
}
var m = 10;
var prog = ("programming");
var java = ("javascript");
var vue = ("vue js");
for (var i = 10; i <= m; i++) {
    console.log(i + " - I love " + prog );
    if (i % 3 === 0) {
      console.log('==========');
    }
  }
  // jawaban soal 4
